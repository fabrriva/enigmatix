import React from 'react';
import ReactDOM from 'react-dom';
import Spinner from '../components/spinner';
import { getEnigme, postEnigmeReponse } from '../actions/enigme';
import Enigme from '../components/enigme';
import { isEmpty } from '../utils';

class EnigmeController extends React.Component {

    constructor(props) {
        super();

        this.state = {
            enigme: {},
            hasError: false,
            showSuccess: false,
        };

        getEnigme(props.current_enigme, this.setEnigme.bind(this));
    }

    setEnigme(response) {
        this.setState({ enigme: response });
        this.reset();
    }

    onEnigmeReponse(server_response) {
        if (server_response.valid == true) {
            //valid, continue
            //show success
            this.setState({ hasError: false, showSuccess: true });
            //load next enigme
            getEnigme(server_response.next_enigme, this.setEnigme.bind(this));
        } else {
            //invalid, stop
            this.setState({ hasError: true, showSuccess: false });
        }
    }

    reset() {
        this.setState({ hasError: false, showSuccess: false });
        $('#inputEnigmeAnswer').val('');
        this.refs.enigme.resetSubmitButton();
    }

    render() {
        if (isEmpty(this.state.enigme)) {
            return <Spinner />
        } else {
            return <Enigme enigme={this.state.enigme} onEnigmeSubmit={(enigme_reponse) => {
                postEnigmeReponse(this.state.enigme, enigme_reponse, (server_response) => {
                    this.onEnigmeReponse(server_response);
                })
            }} hasError={this.state.hasError} showSuccess={this.state.showSuccess} ref="enigme" />
        }
    }
}

export default EnigmeController;