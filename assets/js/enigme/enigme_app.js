import React from 'react';
import ReactDOM from 'react-dom';
import { isEmpty } from '../utils';
import EnigmeMenu from './enigme_menu';
import EnigmeController from './enigme_controller';

class EnigmeApp extends React.Component {

    constructor() {
        super();

        this.state = {
            current_enigme: {},
        };
    }

    openEnigme(enigme_ordre) {
        this.setState({ current_enigme: enigme_ordre });
    }

    render() {
        if (isEmpty(this.state.current_enigme)) {
            return <EnigmeMenu openEnigme={this.openEnigme.bind(this)} />
        } else {
            return <EnigmeController current_enigme={this.state.current_enigme} />
        }
    }
}

ReactDOM.render(<EnigmeApp />, document.getElementById('root_enigme'));