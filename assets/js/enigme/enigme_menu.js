import React from 'react';
import { getEnigmes } from '../actions/enigme';
import Spinner from '../components/spinner';
import { isMobile } from 'mobile-device-detect';

class EnigmeMenu extends React.Component {
    constructor() {
        super();

        this.state = {
            enigmes: [],
            maxPerLine: isMobile ? 4 : 10,
        };

        getEnigmes(this.setEnigmes.bind(this));
    }

    setEnigmes(response) {
        this.setState({ enigmes: response })
    }

    render() {
        if (this.state.enigmes.length == 0) {
            return <Spinner />
        } else {
            return (
                <div class="container d-flex justify-content-center">
                    <div class="row">
                        {this.state.enigmes.map((enigme, index) => {
                            return (
                                <div key={index} class="col col-no-espace">
                                    <button type="button" class="btn btn-primary btn-lg enigme-menu-button" onClick={() => this.props.openEnigme(enigme.ordre)}>{enigme.ordre}</button>
                                </div>
                            )
                        })}
                    </div>
                </div>
            );
        }
    }
}

export default EnigmeMenu;