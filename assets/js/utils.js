export function isEmpty(object) {
    return typeof object == 'object' && Object.entries(object).length == 0;
}