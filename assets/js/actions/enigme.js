import axios from 'axios';
import { API_ENDPOINT } from '../config';

export function getEnigmes(callback) {
    axios.get(`${API_ENDPOINT}/enigmes`).then((response) => {
        callback(response.data);
    })
}

export function getEnigme(ordre, callback) {
    axios.get(`${API_ENDPOINT}/enigme/${ordre}`).then((response) => {
        callback(response.data);
    })
}

export function postEnigmeReponse(enigme, reponse, callback) {
    axios.post(`${API_ENDPOINT}/enigme_reponse`, {
        enigme: enigme,
        reponse: reponse
    }).then((response) => {
        callback(response.data);
    })
}