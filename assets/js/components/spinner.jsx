import React from 'react';

class Spinner extends React.Component {
    render() {
        return (
            <div class="text-center">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        );
    }
}

export default Spinner;