import React from 'react';

class Enigme extends React.Component {
    constructor() {
        super();

        this.state = {
            isSending: false
        };
    }

    resetSubmitButton() {
        this.setState({ isSending: false });
    }

    render() {
        let cardClass = "card";
        if (this.props.hasError) {
            cardClass = "card border-danger";
        }
        if (this.props.showSuccess) {
            cardClass = "card border-success";
        }

        return (
            <div class={cardClass}>
                <h1 class="card-header">Enigme {this.props.enigme.ordre}</h1>
                <div class="card-body">
                    <h5 class="card-title">todo timer</h5>
                    <p class="card-text">{this.props.enigme.question}</p>
                    <form>
                        <label class="form-control-label" for="inputEnigmeAnswer">Réponse:</label>
                        <input type="number" class="form-control" id="inputEnigmeAnswer" />

                        {!this.state.isSending && <button class="btn btn-primary" onClick={() => {
                            this.setState({ isSending: true })
                            this.props.onEnigmeSubmit($('#inputEnigmeAnswer').val());
                        }}>Valider</button>}
                        {this.state.isSending && <button class="btn btn-primary" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement...
                        </button>}
                    </form>
                </div>
            </div>
        );
    }
}

export default Enigme;