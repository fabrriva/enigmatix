<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Enigme;
use App\Repository\EnigmeRepository;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class EnigmeController extends AbstractController
{
    /**
     * @Route("/enigme", name="enigme")
     */
    public function index()
    {
        return $this->render('enigme/index.html.twig');
    }

    /**
     * @Route("/game/enigme", name="start_enigme")
     */
    public function start() {
        return $this->render('enigme/start.html.twig');
    }

}