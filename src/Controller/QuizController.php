<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class QuizController extends AbstractController
{
    /**
     * @Route("/quiz", name="quiz")
     */
    public function index()
    {
        return $this->render('quiz/index.html.twig', [
            'controller_name' => 'QuizController',
        ]);
    }

    /**
     * @Route("/game/quiz", name="start_quiz")
     */
    public function start() {
        $enigme = $this->getDoctrine()->getRepository(Enigme::class)->findOneBy(['ordre' => 1]);

        return $this->render('enigme/index.html.twig', [
            'enigme' => $enigme,
        ]);
    }

}
