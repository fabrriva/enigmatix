<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Enigme;
use App\Repository\EnigmeRepository;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/enigmes"), methods={"GET"})
     */
    public function getEnigmes()
    {
        /** @var EnigmeRepository $enigmeRepo */
        $enigmeRepo = $this->getDoctrine()->getManager()->getRepository(Enigme::class);

        $enigme = $enigmeRepo->findAll(['ordre' => 'ASC']);

        $data =  $this->get('serializer')->serialize($enigme, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['id', 'reponse', 'question']
        ]);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/api/enigme/{ordre}"), methods={"GET"})
     */
    public function getEnigme(int $ordre)
    {
        /** @var EnigmeRepository $enigmeRepo */
        $enigmeRepo = $this->getDoctrine()->getManager()->getRepository(Enigme::class);

        $enigme = $enigmeRepo->findOneBy(['ordre' => $ordre]);

        $data =  $this->get('serializer')->serialize($enigme, 'json', [
            AbstractNormalizer::IGNORED_ATTRIBUTES => ['id', 'reponse']
        ]);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/api/enigme_reponse"), methods={"POST"})
     */
    public function postEnigmeResponse(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $enigme_ordre = $data['enigme']['ordre'];
        $enigme_reponse = $data['reponse'];

        if (!isset($enigme_ordre) || !isset($enigme_reponse)) {
            throw new \Exception('missing parameter');
        }

        /** @var EnigmeRepository $enigmeRepo */
        $enigmeRepo = $this->getDoctrine()->getManager()->getRepository(Enigme::class);
        $db_enigme = $enigmeRepo->findOneBy(['ordre' => $enigme_ordre]);

        $reponse = [];
        $reponse['valid'] = false;
        if ($db_enigme->getReponse() == $enigme_reponse) {
            $reponse['valid'] = true;
            $reponse['next_enigme'] = $enigme_ordre + 1;
            //todo token for load next enigme
        }

        return new JsonResponse($reponse);
    }

}