<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuizReponse", mappedBy="quiz")
     */
    private $quiz_reponses;

    public function __construct()
    {
        $this->quiz_reponses = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getOrdre().". ".$this->getQuestion()." #".$this->getId();
    }

    public function nombreReponses() {
        return count($this->getQuizReponses());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * @return Collection|QuizReponse[]
     */
    public function getQuizReponses(): Collection
    {
        return $this->quiz_reponses;
    }

    public function addQuizReponse(QuizReponse $quizReponse): self
    {
        if (!$this->quiz_reponses->contains($quizReponse)) {
            $this->quiz_reponses[] = $quizReponse;
            $quizReponse->setQuiz($this);
        }

        return $this;
    }

    public function removeQuizReponse(QuizReponse $quizReponse): self
    {
        if ($this->quiz_reponses->contains($quizReponse)) {
            $this->quiz_reponses->removeElement($quizReponse);
            // set the owning side to null (unless already changed)
            if ($quizReponse->getQuiz() === $this) {
                $quizReponse->setQuiz(null);
            }
        }

        return $this;
    }

}
